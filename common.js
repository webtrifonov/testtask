$(document).ready(function () {

var countryTemplate=$(".country_template");
var countryWrapper=$("#wrapper_list");
var sideWrapper=$("#side_wrapper");
var favCountries = {};
/*
*   Отображение избранного после перезагрузки
*/
function localStorageDelete(){
    for (var i = 0; i < localStorage.length; ++i) {
        var lsKey = localStorage.key(i);
        if((lsKey.substr(0,3))=='fav'){
            sideWrapper.append("<li id='"+lsKey+"'><p>"+localStorage[lsKey]+
                               "</p><button type='button' class='del' id='del"+lsKey+"'>Удалить</button></li>");
            sideWrapper.find('#del'+lsKey).click(function (e) {
                e.preventDefault();
                localStorage.removeItem(lsKey);
                sideWrapper.find('#fav'+lsKey).remove();
                location.reload();
            });
        }
    }
}
function getBordersName(ISO){
    var bordersName=$.ajax({
        url: 'https://restcountries.eu/rest/v2/alpha/'+ISO,
        type: "GET",
        success: function (result) {
            return result;
        }
    });
    return bordersName;
}

/**
 * Генерирует данные для шаблона вывода информации о странах
 *
 * @param {result} javascript object - объект с данными о странах 
 */
function generateCountry (result) {
    result.forEach(function (item) {
        var countryRecord = countryTemplate.clone(true);
        countryRecord.attr("id",'item'+item.alpha3Code);
        countryRecord.find('.c_title').html(item.nativeName);
        countryRecord.find('.c_img').attr("src",item.flag);
        countryRecord.find('.c_code').html(item.alpha3Code);
        countryRecord.find('.c_lang').html(item.languages.map(function (item2) {
            return item2.name;
        }).join(' - '));
        item.borders.map(function (item3) {
            countryRecord.find('.c_border').html('');
            getBordersName(item3).then(function(result){
                countryRecord.find('.c_border').append('<a onclick="getBorderCountry(\''+result.name+'\')" href="#">'+result.name+'</a>').append(' ');
            });
        });
        //Кнопка "В избранное"
        if (localStorage.getItem('fav'+item.alpha3Code)==item.name){
            countryRecord.find('.fav_add').hide();
            countryRecord.find('.fav_del').show();
        } else {
            countryRecord.find('.fav_del').hide();
            countryRecord.find('.fav_add').show();
        }
        favCountries[item.alpha3Code]=item;
        var favAlpha3Code = favCountries[item.alpha3Code].alpha3Code;
        var favName = favCountries[item.alpha3Code].name;
        console.log(favAlpha3Code);
        countryRecord.find('.fav_add').click(function (e) {
            e.preventDefault();
            $(this).hide();
            countryRecord.find('.fav_del').show();
            sideWrapper.append("<li id='fav"+favAlpha3Code+"'><p>"+
                                favName+"</p><button type='button' class='del' id='del"+
                                favAlpha3Code+"'>Удалить</button></li>");
            localStorage.setItem('fav'+favAlpha3Code,favName);
            //Кнопка "удалить" в сайдбаре
            sideWrapper.find('#del'+favAlpha3Code).click(function (e) {
                e.preventDefault();
                localStorage.removeItem('fav'+favAlpha3Code);
                sideWrapper.find('#fav'+favAlpha3Code).remove();
                countryRecord.find('.fav_del').hide();
                countryRecord.find('.fav_add').show();
            });
        });
        //Кнопка "Удалить из избранного"
        countryRecord.find('.fav_del').click(function (e) {
            e.preventDefault();
            $(this).hide();
            countryRecord.find('.fav_add').show();
            var favAlpha3Code = favCountries[item.alpha3Code].alpha3Code;
            sideWrapper.find('#fav'+favAlpha3Code).remove();
            localStorage.removeItem('fav'+favAlpha3Code);
        });

        countryWrapper.append(countryRecord);
    });
}
window.getBorderCountry = function(resultName){
    $.ajax({
        url: 'https://restcountries.eu/rest/v2/name/'+resultName,
        type: "GET",
        success: function (result) {
            countryWrapper.empty();
            countryTemplate.removeClass('part');
            countryTemplate.addClass('full');
            generateCountry(result);
        }
    });
}
/**
 * Генерирует данные для шаблона вывода информации о странах
 *
 * @param {result} javascript object - объект с данными о странах
 */
function showButtonHandler (result) {
    result.forEach(function (item) {
        var show=$("#item"+item.alpha3Code+" .show");
        show.click(function () {
             if($("#item"+item.alpha3Code+" .full_info").is(':visible')){
                 show.html("Показать ↓");
             } else{
                 show.html("Скрыть ↑");
             }
           $("#item"+item.alpha3Code+" .full_info").slideToggle();
        });
    });
}

$("#search_but").click(function (e) {
    e.preventDefault();
    var inputText=$("#search_field").val();
    $.ajax({
        url: 'https://restcountries.eu/rest/v2/name/'+inputText,
        type: "GET",
        success: function (result) {
            console.log(result);
            if(result.length==1){
                countryWrapper.empty();
                countryTemplate.removeClass('part');
                countryTemplate.addClass('full');
                generateCountry(result);
            } else {
                countryWrapper.empty();
                countryWrapper.append("<h1>Cписок стран</h1>");
                countryTemplate.removeClass('full');
                countryTemplate.addClass('part');
                generateCountry(result);
                showButtonHandler(result);
            }
        }
    });

});
localStorageDelete();
});




